﻿using System;

namespace Phimath.Telemetry.TestApp
{
    internal sealed class DummyTelemetryData : ISerializableTelemetryData
    {
        public DummyTelemetryData()
        {
            LastUpdated = DateTimeOffset.Now;
        }

        public DateTimeOffset LastUpdated { get; }

        public string Serialize()
        {
            return "{\"" + nameof(LastUpdated) + "\": \"" + LastUpdated + "\"}";
        }
    }

    internal static class Program
    {
        private static void Main(string[] args)
        {
            var telemetry = Telemetry.Initialize();

            Console.WriteLine("Hello World!");
            Console.WriteLine("----------");
            Console.WriteLine("If you can read this, than initialization of the telemetry was successful!");
            Console.WriteLine();
            Console.WriteLine($"The app ID for this app is: {telemetry.AppId.ToString()}");
            Console.WriteLine($"Installations are saved under: {telemetry.DataFolder}");

            Console.WriteLine("----------");
            var firstGet = telemetry.TryGetInstallationId(out var firstGetGuid);
            Console.Write("Is there a previous installation? ");
            if (firstGet)
            {
                Console.WriteLine($"Yes! => {firstGetGuid.ToString()}");
            }
            else
            {
                Console.WriteLine("No.");
            }

            Console.WriteLine($"GetOrCreateInstallationId returns: {telemetry.GetOrCreateInstallationId().ToString()}");


            Console.WriteLine("----------");
            var dummyTelemetryData = new DummyTelemetryData();
            Console.WriteLine($"Looking at a dummy object of type {nameof(DummyTelemetryData)}:");
            Console.WriteLine(
                $"   {nameof(DummyTelemetryData.LastUpdated)} = {dummyTelemetryData.LastUpdated.ToString()}");
            Console.WriteLine($"   Serialize() = {dummyTelemetryData.Serialize()}");
            telemetry.SubmitTelemetry(dummyTelemetryData);
        }
    }
}