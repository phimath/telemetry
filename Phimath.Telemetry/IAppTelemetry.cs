using System;

namespace Phimath.Telemetry
{
    public interface IAppTelemetry
    {
        /// <summary>
        ///     Gets the app ID.
        /// </summary>
        Guid AppId { get; }

        string DataFolder { get; }

        /// <summary>
        ///     Tries to get the installation ID, if present.
        /// </summary>
        /// <param name="installationId">The installation ID, if found, or <see cref="Guid.Empty" />.</param>
        /// <returns>True, if ID is found, otherwise false.</returns>
        /// <exception cref="ArgumentOutOfRangeException">When the <see cref="DataFolder" /> contains more than one installation.</exception>
        bool TryGetInstallationId(out Guid installationId);

        /// <summary>
        ///     Gets an existing installation ID or creates a new one, if none exists.
        /// </summary>
        /// <returns>A valid installation ID.</returns>
        /// <exception cref="ArgumentOutOfRangeException">When the <see cref="DataFolder" /> contains more than one installation.</exception>
        Guid GetOrCreateInstallationId();

        void SubmitTelemetry(ISerializableTelemetryData data);
    }
}