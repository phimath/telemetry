using System;
using System.IO;
using System.Runtime.Serialization;

namespace Phimath.Telemetry
{
    public abstract class TelemetryException : Exception
    {
        protected TelemetryException()
        {
        }

        protected TelemetryException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        protected TelemetryException(string message) : base(message)
        {
        }

        protected TelemetryException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

    public sealed class MultipleInstallationsFoundException : TelemetryException
    {
        internal MultipleInstallationsFoundException() : base("More than one installation was found")
        {
        }
    }

    public sealed class InvalidInstallationFileNameException : TelemetryException
    {
        internal InvalidInstallationFileNameException(FileInfo fileInfo)
            : base($"Installation file was not in a GUID format: {fileInfo.FullName}")
        {
        }
    }

    public sealed class InvalidTokenResponseException : TelemetryException
    {
        internal InvalidTokenResponseException(Guid sentToken, string response)
            : base($"Telemetry was submitted under token {sentToken.ToString().ToLowerInvariant()}, " +
                   $"but server responded with token {response}")
        {
        }
    }

    public sealed class UnspecifiedErrorException : TelemetryException
    {
        internal UnspecifiedErrorException(int statusCode, string content)
            : base($"An unexpected response was returned by the server. Status code: {statusCode}. Content: {content} ")
        {
        }
    }

    public sealed class UnknownAppException : TelemetryException
    {
        internal UnknownAppException(string content) : base($"Your app is unknown to the server: {content}")
        {
        }
    }

    public sealed class BadTokenException : TelemetryException
    {
        internal BadTokenException(string token) : base($"The server declared your token as invalid: {token}")
        {
        }
    }

    public sealed class BadJsonException : TelemetryException
    {
        internal BadJsonException(string message) : base($"The server threw an error processing your JSON: {message}")
        {
        }
    }

    public sealed class TokenUsedTwiceException : TelemetryException
    {
        internal TokenUsedTwiceException(string token)
            : base($"You cannot use a token to save more than one telemetry datum: {token}")
        {
        }
    }

    public sealed class ServerException : TelemetryException
    {
        internal ServerException(string problemJson)
            : base($"Server cannot handle your request. application/problem+json is: {problemJson}")
        {
        }
    }
}