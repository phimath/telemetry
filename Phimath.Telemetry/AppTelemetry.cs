using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Phimath.Telemetry
{
    internal static class HttpContentExtensions
    {
        public static string ContentAsString(this HttpResponseMessage message)
        {
            return message.Content.ReadAsStringAsync().GetAwaiter().GetResult();
        }
    }

    internal sealed class AppTelemetry : IAppTelemetry
    {
        private const string JsonMediaType = "application/json";

        private const string VendorDirectoryName = "Phimath";
        private const string NamepsaceDirectoryName = "Telemetry";
        private const string InstallationFileExtension = ".installation.json";
        private const string InstallationFilePattern = "*" + InstallationFileExtension;

        private static readonly string LocalAppDataPath = Environment.GetFolderPath(
            Environment.SpecialFolder.LocalApplicationData,
            Environment.SpecialFolderOption.Create
        );

        private static readonly string ExecutingVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString(3);

        private static readonly Regex InstallationFileRegex = new Regex(
            "^([0-9a-f]{8}-(?>[0-9a-f]{4}-){3}[0-9a-f]{12})" + InstallationFileExtension + "$",
            RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled
        );

        private readonly DirectoryInfo _dataFolderHandle;

        public AppTelemetry(Guid appId)
        {
            AppId = appId;
            DataFolder = Path.Combine(BaseSaveLocation, appId.ToString().ToLowerInvariant());
            _dataFolderHandle = Directory.CreateDirectory(DataFolder);
        }

        private static string BaseSaveLocation =>
            Path.Combine(LocalAppDataPath, VendorDirectoryName, NamepsaceDirectoryName);

        public Guid AppId { get; }

        public bool TryGetInstallationId(out Guid installationId)
        {
            var files = _dataFolderHandle.GetFiles(InstallationFilePattern);
            if (files.Length == 0)
            {
                installationId = Guid.Empty;
                return false;
            }

            if (files.Length > 1)
            {
                throw new MultipleInstallationsFoundException();
            }

            var file = files[0];
            var nameMatch = InstallationFileRegex.Match(file.Name);
            if (!nameMatch.Success)
            {
                throw new InvalidInstallationFileNameException(file);
            }

            installationId = Guid.Parse(nameMatch.Groups[1].Value);
            return true;
        }

        public Guid GetOrCreateInstallationId()
        {
            if (TryGetInstallationId(out var existing))
            {
                return existing;
            }

            var newGuid = Guid.NewGuid();
            var newGuidFileName = $"{newGuid.ToString().ToLowerInvariant()}{InstallationFileExtension}";
            File.Create(Path.Combine(_dataFolderHandle.FullName, newGuidFileName));
            return newGuid;
        }

        public void SubmitTelemetry(ISerializableTelemetryData data)
        {
            var installationId = GetOrCreateInstallationId();
            var uriString = $"https://api.phimath.de/telemetry/{AppId.ToString()}/{installationId.ToString()}/";

            using (var httpClient = new HttpClient {BaseAddress = new Uri(uriString, UriKind.Absolute)})
            {
                // Get token first
                var tokenResponse = httpClient.GetAsync("token").GetAwaiter().GetResult();
                string tokenResponseContent;

                switch (tokenResponse.StatusCode)
                {
                    // This is allowed
                    case HttpStatusCode.OK:
                        tokenResponseContent = tokenResponse.ContentAsString();
                        break;
                    // This happens when there is an exception where none should be
                    case HttpStatusCode.InternalServerError: throw Problem(tokenResponse);
                    default: throw UnspecifiedError(tokenResponse);
                }

                var telemetryToken = Guid.Parse(tokenResponseContent);

                var telemetryContent = BuildTelemetryContent(telemetryToken, data);
                var telemetryResponse = SendTelemetry(httpClient, telemetryContent);
                switch (telemetryResponse.StatusCode)
                {
                    // This is allowed
                    case HttpStatusCode.OK:
                        CheckTokensOrThrow(telemetryToken, tokenResponse.ContentAsString());
                        break;
                    // This happens if a token is used twice
                    case HttpStatusCode.PreconditionFailed: throw TokenUsedTwice(telemetryToken);
                    // This happens if there is an error parsing the sent JSON on the server
                    case HttpStatusCode.BadRequest: throw BadJson(telemetryResponse);
                    // This happens if the server cannot validate the token
                    case HttpStatusCode.Unauthorized: throw BadToken(telemetryToken);
                    // This happens when the app ID is unknown to the server
                    case HttpStatusCode.NotFound: throw UnknownApp(telemetryResponse);
                    // This happens when there is an exception where none should be
                    case HttpStatusCode.InternalServerError: throw Problem(telemetryResponse);
                    default: throw UnspecifiedError(telemetryResponse);
                }
            }
        }

        public string DataFolder { get; }

        private static ServerException Problem(in HttpResponseMessage message)
        {
            return new ServerException(message.ContentAsString());
        }

        private static BadTokenException BadToken(in Guid token)
        {
            return new BadTokenException(token.ToString().ToLowerInvariant());
        }

        private static TokenUsedTwiceException TokenUsedTwice(in Guid token)
        {
            return new TokenUsedTwiceException(token.ToString().ToLowerInvariant());
        }

        private static BadJsonException BadJson(in HttpResponseMessage message)
        {
            return new BadJsonException(message.ContentAsString());
        }

        private static UnspecifiedErrorException UnspecifiedError(in HttpResponseMessage message)
        {
            return new UnspecifiedErrorException((int) message.StatusCode, message.ContentAsString());
        }

        private static UnknownAppException UnknownApp(in HttpResponseMessage message)
        {
            return new UnknownAppException(message.ContentAsString());
        }

        private static StringContent BuildTelemetryContent(in Guid token, in ISerializableTelemetryData data)
        {
            return new StringContent(
                "{\"token\": \"" + token.ToString().ToLowerInvariant() + "\", \"content\": " + data.Serialize() + "}",
                Encoding.UTF8,
                JsonMediaType
            );
        }

        private static HttpResponseMessage SendTelemetry(in HttpClient httpClient, in StringContent telemetryContent)
        {
            return httpClient.PostAsync("", telemetryContent).GetAwaiter().GetResult();
        }

        private static void CheckTokensOrThrow(in Guid telemetryToken, in string telemetryResponseContent)
        {
            if (Guid.Parse(telemetryResponseContent) != telemetryToken)
            {
                throw new InvalidTokenResponseException(telemetryToken, telemetryResponseContent);
            }
        }
    }
}