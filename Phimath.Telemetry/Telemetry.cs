using System;
using System.Collections.Concurrent;
using System.Reflection;

namespace Phimath.Telemetry
{
    public static class Telemetry
    {
        private static readonly ConcurrentDictionary<Guid, IAppTelemetry> _telemetries =
            new ConcurrentDictionary<Guid, IAppTelemetry>();

        /// <summary>
        ///     Initializes the telemetry state for your app.
        ///     Call this function from the assembly on which you defined the <see cref="TelemetryAppIdAttribute" />.
        /// </summary>
        /// <seealso cref="TelemetryAppIdAttribute" />
        public static IAppTelemetry Initialize()
        {
            var caller = Assembly.GetCallingAssembly();
            var appIdAttribute = caller.GetCustomAttribute<TelemetryAppIdAttribute>();
            if (appIdAttribute == null)
            {
                throw new InvalidOperationException(
                    $"You must specify the {typeof(TelemetryAppIdAttribute).FullName} attribute on your assembly with the app GUID");
            }

            return _telemetries.GetOrAdd(appIdAttribute.Guid, guid => new AppTelemetry(guid));
        }
    }
}