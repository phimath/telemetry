namespace Phimath.Telemetry
{
    public interface ISerializableTelemetryData
    {
        string Serialize();
    }
}