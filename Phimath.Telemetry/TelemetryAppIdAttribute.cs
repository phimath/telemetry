﻿using System;

namespace Phimath.Telemetry
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public sealed class TelemetryAppIdAttribute : Attribute
    {
        public TelemetryAppIdAttribute(Guid guid)
        {
            Guid = guid;
        }

        public TelemetryAppIdAttribute(string guidString) : this(Guid.Parse(guidString))
        {
        }

        internal Guid Guid { get; }
    }
}